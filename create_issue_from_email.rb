#!/usr/bin/env ruby

# Processes a POP inbox and creates JIRA issues via the REST API.

# Requires JIRA >= 5.0.
# Requires the following Ruby gems:
# - json (for Ruby < 1.9)
# - mailman
# - rest-client

require 'rubygems'
require 'json'
require 'mailman'
require 'rest_client'

# JIRA configuration
JIRA_INSTANCE = 'https://username:password@jira.example.com'
PROJECT_KEY = "TEST"
ISSUE_TYPE = "Bug"

# Mail configuration
Mailman.config.pop3 = {
  :username => 'example@gmail.com',
  :password => 'mypassword',
  :server   => 'pop.gmail.com',
  :port     => 995,
  :ssl      => true
}

# Only run once per script invocation.
Mailman.config.poll_interval = 0

Mailman::Application.run do
  # Only create issues from mail explicitly sent to this address.
  to 'example@gmail.com' do
    issue = { :fields => {
              :project => { :key => PROJECT_KEY },
              :issuetype => { :name => ISSUE_TYPE },
              :summary => message.subject,
              :description => message.body } }
    resource = JIRA_INSTANCE + "/rest/api/2/issue/"
    RestClient.log = $stdout
    begin
      response = RestClient.post resource, issue.to_json, :content_type => 'application/json', :accept => :json
    rescue Exception => e
      e.inspect
    else
      puts "Created issue: #{JSON.parse(response.body)["key"]}"
    end
  end
end
